# node-quicksort

Quicksort algorithm implementation in nodejs. The array orders arrays of numbers.

There is a server with an endpoint that orders arrays of numbers using the algorithm (index.js). You can import *node-quicksort.postman_collection.json* collection to test the algorithm.

## Commands

All commands are executed from the root. 

### Server up

```
node index.js
```

### Build docker

```
docker build --no-cache --tag=node-quicksort:1.0.0 .
```

### Run docker
```
docker run -p 8080:8080 -dit --name node-quicksort node-quicksort:1.0.0
```

## Modify the ordering logic

By default it is ordered from highest to lowest but you can overwrite **logicalComparison** to modify the comparison logic, for example we could sort by the number that in binary has more ones, and in case of same number of ones, prioritize the smallest number, for this we would add to our index.js this code::

```
    ...
require('./primitive-hydration.js')

Number.prototype.logicalComparison = function(number){
    let numberA = this.toBinary().countPositives()
    let numberB = number.toBinary().countPositives()
    let factor = 1
    if (numberA === numberB){
        numberA = this
        numberB = number
        factor = -1
    }
    if (numberA === numberB){
        return 0
    } else if(numberA > numberB){
        return 1 * factor
    }
    return -1 * factor
}

Number.prototype.toBinary = function(){
    return this.toString(2)
}

String.prototype.countPositives = function(){
    return this.replace(/0/g, '').length
}

const sendToClient = function(response,message,status){
    ...
```

In this way this data entry:

[1,15,5,7,3]

We would get this output:

[15,7,3,5,1]

As: 

15 in binary is: 1111 (4 positive)
7 in binary is: 111 (3 positive)
3 in binary is: 11 (2 positive)
5 in binary is: 101 (2 positive and greater than 3)
1 in binary is: 1 (1 positive)