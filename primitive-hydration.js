'use strict'

Array.prototype.quicksort = function(){
    const separate = this.separate()
    if (separate.left.length > 1){
        separate.left = separate.left.quicksort()
    }
    if (separate.right.length > 1){
        separate.right = separate.right.quicksort()
    }
    separate.left.push(separate.pivot)
    return separate.left.concat(separate.right)
}

Array.prototype.separate = function(){
    let pivotKey = this.pivot()
    let pivot = this[pivotKey]
    const left = []
    const right = []
    for (let i in this){
        if (i == pivotKey || typeof this[i] !== 'number'){
            continue
        }
        if (this[i].logicalComparison(pivot) === 1){
            left.push(this[i])
        } else {
            right.push(this[i])
        }
    }
    return { left, right, pivot }
}

Array.prototype.pivot = function(){
    const length = this.length
    if (length <= 3) {
        return 0
    }

    const mediumKey = Math.round((length - 1) / 2)
    const leftValue = this[0]
    const rightValue = this[length - 1]
    const mediumValue = this[mediumKey]

    let leftComparison = leftValue.logicalComparison(mediumValue)
    let rightComparison = rightValue.logicalComparison(mediumValue)
    if ((leftComparison + rightComparison) === 0){
        return mediumKey
    }
    let extremeComparison = leftValue.logicalComparison(rightValue)
    if (leftComparison === -1 && extremeComparison === -1){
        return length - 1
    }
    return 0
}

Number.prototype.logicalComparison = function(number){
    if (this === number){
        return 0
    } else if (this > number){
		return 1
	}
	return -1
}
