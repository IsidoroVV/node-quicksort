FROM node:10
WORKDIR /usr/src/app
COPY index.js .
COPY primitive-hydration.js .

EXPOSE 8080
CMD [ "node", "index.js" ]