'use strict'

const http = require('http')
require('./primitive-hydration.js')

const sendToClient = function(response,message,status){
  	response.writeHead(status, {'Content-Type': 'application/json'});
  	response.write(JSON.stringify(message));
  	response.end();
}

http.createServer(function (req, res) {
	if (req.url === '/sort' && req.method === 'POST') {
	    let body = '';
	    req.on('data', chunk => {
	        body += chunk.toString()
	    });
	    req.on('end', () => {
			try {
				body = JSON.parse(body)	
			}
			catch(error) {
			  sendToClient(res,{ error: 'JSON malformed, example: [2,45,8,9,256,257]'},400)
			  return
			}
			let validatedInput = []
		    for (let i in body){
		    	if (typeof body[i] === 'number'){
		    		validatedInput.push(body[i])
		    	}
		    }
	        sendToClient(res,validatedInput.quicksort(),200)
	    });
	} else {
		sendToClient(res,{ error: 'The requested resource does not exist, you should use POST /sort'},404)
	}
  	
}).listen(8080);

